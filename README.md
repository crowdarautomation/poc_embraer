**Setting up Embraer Project**
---
##First, download and install tools. 
 (alternatively you can download the resources from Team Drive "Embraer/dev" ) 

 0. Install jdk8 (https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
 1. Install CAFM aplication, get instructions from embraer Team Drive (https://drive.google.com/open?id=1GJ934-4FUBtvCNneikmX_s6au-3T2yES)
 2. Install QT (https://download.qt.io/archive/qt/5.3/5.3.1/qt-opensource-windows-x86-msvc2012_opengl-5.3.1.exe)
 3. Install windows 10 sdk (https://developer.microsoft.com/en-us/windows/downloads/windows-10-sdk)
           
 4. Download last winAppDriver release (https://github.com/Microsoft/WinAppDriver/releases)
 5. Download eclipse
 6. Download smartgit or alternative tool to interact with git repository
---
	
##Now, configure the context to execute tests.
 1. **Restart computer!**
 2. run **CAFM aplication**
 3. run **inspect.exe** to access to the aplication tree elements "C:\Program Files (x86)\Windows Kits\10\bin\10.0.17134.0\x86\inspect.exe"
 4. run **winAppDriver.exe** from command line
--- 

##Configure IDE to write and execute automation tests.
 1. from smartgit, clone this repository.
 2. run eclipse
 	- import exiting maven project
	- select cloned repository location
	- over root of project -> right click -> run as junit.
---	
	
 **Fabulous!!**
---
---
