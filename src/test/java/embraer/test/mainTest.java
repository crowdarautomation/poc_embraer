package embraer.test;

import java.io.File;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.sun.jna.platform.win32.WinDef.SCODEByReference;

import embraer.DriverManager;
import embraer.ScreenshotCapturer;
import embraer.window.EnrouteWindow;
import embraer.window.common.AlertWindow;
import embraer.window.common.ReportWindow;
import embraer.window.common.StatusBar;
import embraer.window.common.ToolBar;
import embraer.window.common.ToolBar.ToolbarOption;

public class mainTest {
	
	
	 @Test
	    public void enroutePositiveTest(){
	        ToolBar.goTo(ToolbarOption.ENROUTE);
	        EnrouteWindow.clear();
	        
	      //Airplane Data
	        EnrouteWindow.setEnginesModel("10E7");
	        EnrouteWindow.setDatabaseApplicationType("Normal Operation");
	        
	      //Airplane Configuration
	        EnrouteWindow.setIceAccretion("YES");
	        EnrouteWindow.setAntiIceStatus("OFF");
	        
	      //Calculation Options
	        EnrouteWindow.setWeight("27000");
	        EnrouteWindow.setLoadFactor("1");
	        EnrouteWindow.setDragIndex("1");
	        EnrouteWindow.setSpeedOption("Given Speed");
	        EnrouteWindow.setSpeedType("Mach");
		 
	        EnrouteWindow.setSpeedValue("0.5");        
	        //assertion: status bar shown valid speed range  
	        Assert.assertTrue(StatusBar.getText().contains("Range: 0 to 0.82"));
	        	        
	        //Environmental Data
	        EnrouteWindow.setPreassureAltitude("1");
	        EnrouteWindow.setFrom("1");
	        EnrouteWindow.setTo("1");
	        
	        EnrouteWindow.calculate();

	        //Assertion: alert shown when speed above the maximun speed 
	        Assert.assertTrue("Alert message not valid.", AlertWindow.getMessage().contains("Input speed above the maximum speed: 0.45"));
	        
	        AlertWindow.clickOk();
	        
	        // assertion: report content
		 	Assert.assertTrue("Invalid report generation.", ReportWindow.getReportContent().contains("Calculation messages:"));

		 	// demo of interactions with the report
		 	System.out.println(ReportWindow.getReportContent());
		 	ScreenshotCapturer.createScreenCapture(DriverManager.getDriver(),"D:\\screenshots\\screenshotExample.png");
		 	
		 	ReportWindow.back();
	        
	    }
	 
	 
	 
	 
	 public void enrouteNegativeTest(){
//		 este va a intentar meter valores fuera de los maximos y minimos. 
	 }
	 

}
