package embraer;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.windows.WindowsDriver;

public class DriverManager {
	
	private static WindowsDriver driver;
	
	private DriverManager() {
	}

	public static WindowsDriver getDriver(){
		if(driver == null) {
			try {
				DesiredCapabilities capabilities = new DesiredCapabilities();
				
				//appTpLevelWindow = NativeWindowHandle(inspector atribute)
				capabilities.setCapability("appTopLevelWindow", "0x1D02C8");
				driver = new WindowsDriver(new URL("http://127.0.0.1:4723"), capabilities);
				driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		}
		return driver;
	}

}
