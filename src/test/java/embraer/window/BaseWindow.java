package embraer.window;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import embraer.DriverManager;

public class BaseWindow {
	public static final String BASE_WINDOW_NAME_XPATH = "//Window[@Name='CAFM - Computerized Airplane Flight Manual']"; 
	
	public static final String EDITABLE_FIELDS_XPATH = "//ComboBox | //Edit";

	protected static void setCheckboxValue(List<WebElement> elements, int index, String option) {
		WebElement field = elements.get(index);
		field.click();
		field.findElement(By.name(option)).click();
	}
	
	protected static void setEditValue(List<WebElement> elements, int index, String value) {
		WebElement field = elements.get(index);
		field.click();
		field.sendKeys(value);
	}
	
}
