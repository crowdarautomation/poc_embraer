package embraer.window;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import embraer.DriverManager;

public class EnrouteWindow extends BaseWindow{

	private static final String AIRPLANE_DATA_SECTION_XPATH = "//Group[@Name='Airplane data']";
	private static final String AIRPLANE_CONFIGURATION_SECTION__XPATH = "//Group[@Name='Airplane configuration']";
	private static final String CALCULATION_OPTIONS_SECTION_XPATH = "//Group[@Name='Calculation options']";
	private static final String ENVIRONMENTAL_DATA_SECTION_XPATH = "//Group[@Name='Environmental data']";
	
	private static final String CLEAR_BUTTON_XPATH = "//Button[@Name='Clear']";
	private static final String CALCULATE_BUTTON_XPATH = "//Button[@Name='Calculate']";
	
	//***********
	//***********  Airplane data
	
	public static void setAirplaneModel(String option){
		setCheckboxValue(getAirplaneDataFields(), 0, option);	
	}
	
	public static void setCertificationType(String option) {
		setCheckboxValue(getAirplaneDataFields(), 1, option);
	}
	
	public static void setEnginesModel(String option) {
		setCheckboxValue(getAirplaneDataFields(), 2, option);
	}

	public static void setDatabaseApplicationType(String option) {
		setCheckboxValue(getAirplaneDataFields(), 3, option);
	}

	//***********
	//***********  Airplane configuration
	
	public static void setIceAccretion(String option) {
		setCheckboxValue(getAirplaneConfigurationFields(), 0, option);
	}

	public static void setAntiIceStatus(String option) {
		setCheckboxValue(getAirplaneConfigurationFields(), 1, option);
	}
	
	//***********
	//***********  Calculation options

	public static void setWeight(String value) {
		setEditValue(getCalculationOptionsFields(), 0, value);
	}
	
	public static void setLoadFactor(String value) {
		setEditValue(getCalculationOptionsFields(), 1, value);
	}
	
	public static void setDragIndex(String value) {
		setEditValue(getCalculationOptionsFields(), 2, value);
	}
	
	public static void setSpeedOption(String option) {
		setCheckboxValue(getCalculationOptionsFields(), 3, option);
	}
	
	public static void setSpeedType(String option) {
		setCheckboxValue(getCalculationOptionsFields(), 4, option);
	}

	public static void setSpeedValue(String value) {
		setEditValue(getCalculationOptionsFields(), 5, value);
	}
	
	//***********
	//***********  Environmental data
	
	public static void setPreassureAltitude(String value) {
		setEditValue(getEnvironmentalDataFields(), 0, value);
		unfocus();
	}
	public static void setFrom(String value) {
		setEditValue(getEnvironmentalDataFields(), 1, value);
		unfocus();
	}
	public static void setTo(String value) {
		setEditValue(getEnvironmentalDataFields(), 2, value);
		unfocus();
	}
	public static void setStep(String value) {
		setEditValue(getEnvironmentalDataFields(), 3, value);
	}
	
	public static void clear(){
		DriverManager.getDriver().findElementByXPath(BASE_WINDOW_NAME_XPATH + CLEAR_BUTTON_XPATH).click();
	}
	
	public static void calculate(){
		DriverManager.getDriver().findElementByXPath(BASE_WINDOW_NAME_XPATH + CALCULATE_BUTTON_XPATH).click();
	}
	
	
	private static List<WebElement> getAirplaneDataFields(){
		return DriverManager.getDriver().findElementByXPath(BASE_WINDOW_NAME_XPATH + AIRPLANE_DATA_SECTION_XPATH)
		.findElements(By.xpath(EDITABLE_FIELDS_XPATH));
	}

	private static List<WebElement> getAirplaneConfigurationFields(){
		return DriverManager.getDriver().findElementByXPath(BASE_WINDOW_NAME_XPATH + AIRPLANE_CONFIGURATION_SECTION__XPATH)
				.findElements(By.xpath(EDITABLE_FIELDS_XPATH));
	}
	
	private static List<WebElement> getCalculationOptionsFields(){
		return DriverManager.getDriver().findElementByXPath(BASE_WINDOW_NAME_XPATH + CALCULATION_OPTIONS_SECTION_XPATH)
				.findElements(By.xpath(EDITABLE_FIELDS_XPATH));
	}

	private static List<WebElement> getEnvironmentalDataFields(){
		return DriverManager.getDriver().findElementByXPath(BASE_WINDOW_NAME_XPATH + ENVIRONMENTAL_DATA_SECTION_XPATH)
				.findElements(By.xpath(EDITABLE_FIELDS_XPATH));
	}
	
	private static void unfocus(){
		DriverManager.getDriver().findElementByXPath(BASE_WINDOW_NAME_XPATH + AIRPLANE_DATA_SECTION_XPATH ).click();
	}
}

//.findElements(By.tagName("Image"));