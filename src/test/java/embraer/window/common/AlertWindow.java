package embraer.window.common;

import embraer.DriverManager;

public class AlertWindow {

	private static final String ALERT_WINDOW_XPATH= "//Window[@Name='CAFM - Computerized Airplane Flight Manual']//Window";

	private static final String MESSAGE_XPATH = ALERT_WINDOW_XPATH + "//Text";
	private static final String BUTTON_OK_XPATH = ALERT_WINDOW_XPATH + "//Button[@Name='OK']";
	
	
	public static String getTitle(){
		return DriverManager.getDriver().findElementByXPath(ALERT_WINDOW_XPATH).getText();
	}

	public static String getMessage(){
		return DriverManager.getDriver().findElementByXPath(MESSAGE_XPATH).getText();
	}

	public static void clickOk(){
		DriverManager.getDriver().findElementByXPath(BUTTON_OK_XPATH).click();
	}

}


	
	
