package embraer.window.common;

import org.openqa.selenium.By;

import embraer.DriverManager;

public final class ToolBar {
	
	private static final String TOOLBAR_XPATH= "//Window[@Name='CAFM - Computerized Airplane Flight Manual']//ToolBar";
	
	public enum ToolbarOption{
		TACKEOFF("Tackeoff"),
		TURN("Turn"),
		ENROUTE("Enroute"),
		LANDING("Landing"),
		AIRPORT("Airport"),
		SETTINGS("Settings"),
		ABOUT("About"),
		CLOSE("Close");
	
		private String toolbarOptionName;

		private ToolbarOption(String toolbarOptionName) {
			this.toolbarOptionName = toolbarOptionName;
		}
		
		public String getName() {
			return toolbarOptionName;
		}
	}
	
	public static void goTo(ToolbarOption option){
		DriverManager.getDriver().findElementByXPath(TOOLBAR_XPATH).findElement(By.name(option.getName())).click();
	}
	
}
