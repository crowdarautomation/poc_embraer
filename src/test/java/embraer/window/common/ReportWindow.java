package embraer.window.common;

import embraer.DriverManager;

public class ReportWindow {
	
	private static final String ALERT_WINDOW_XPATH= "//Window[@Name='CAFM - Computerized Airplane Flight Manual']//Pane//Pane";

	private static final String CONTENT_XPATH= ALERT_WINDOW_XPATH + "//Group//Edit";
	private static final String BUTTON_EXPORT_XPATH = ALERT_WINDOW_XPATH + "//Button[@Name='Export']";
	private static final String BUTTON_RETURN_XPATH = ALERT_WINDOW_XPATH + "//Button[@Name='Return']";
	
	
	public static String getTitle(){
		return DriverManager.getDriver().findElementByXPath(ALERT_WINDOW_XPATH).getText();
	}

	public static String getReportContent(){
		return DriverManager.getDriver().findElementByXPath(CONTENT_XPATH).getText();
	}

	public static void export(){
		DriverManager.getDriver().findElementByXPath(BUTTON_EXPORT_XPATH).click();
	}

	public static void back(){
		DriverManager.getDriver().findElementByXPath(BUTTON_RETURN_XPATH).click();
	}

}
