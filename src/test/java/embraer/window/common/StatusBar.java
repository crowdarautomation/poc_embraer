package embraer.window.common;

import org.openqa.selenium.By.ByXPath;

import embraer.DriverManager;

public final class StatusBar {
	
	private static final String STATUSBAR_XPATH= "//Window[@Name='CAFM - Computerized Airplane Flight Manual']//StatusBar";
	
	public static String getText(){
		return DriverManager.getDriver().findElementByXPath(STATUSBAR_XPATH+ "//Text").getText();
	}
	
}
