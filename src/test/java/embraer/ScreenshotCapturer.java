package embraer;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.RemoteWebDriver;

public class ScreenshotCapturer {

	
	public static void createScreenCapture(RemoteWebDriver driver, String fileLocaton){
		try { 
			
			File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File(fileLocaton));
			
		}catch(IOException ioe){
			ioe.printStackTrace();
		}
	}
}
